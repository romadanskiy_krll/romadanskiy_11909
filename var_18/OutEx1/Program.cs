﻿using System;

namespace OutEX1
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите шестизначное число: ");
            var number = int.Parse(Console.ReadLine());
            var a = number;
            var b = 0;
            while (a > 0)
            {
                b = 10 * b + a % 10;
                a /= 10;
            }
            if (b == number)
                Console.WriteLine("Число является палиндромом");
            else
                Console.WriteLine("Число не является палиндромом");
        }
    }
}
