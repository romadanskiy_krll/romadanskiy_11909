﻿using System;

namespace OutEx3
{
    class Program
    {
        static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            var a = int.Parse(Console.ReadLine());
            var count = 1;
            bool chek  = true;
            while (count <= n-1)
            {
                var b = int.Parse(Console.ReadLine());
                if (a * b > 0) chek = false;
                a = b;
                count++;
            }
            if (chek) Console.WriteLine("yes");
            else Console.WriteLine("no"); 
        }
    }
}
