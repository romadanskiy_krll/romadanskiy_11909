﻿using System;
// +- ну тут без встроенных методов надо было сделать
namespace OutEx2
{
    class Program
    {
        static void Main()
        {
            Console.Write("Введите двоичное число a: ");
            var binaryNumber = Console.ReadLine();
            var a = Convert.ToInt32(binaryNumber, 2);
            Console.Write("Введите натуральное десятичное число k: ");
            var k = int.Parse(Console.ReadLine());
            if (a % k == 0)
                Console.WriteLine("Число а нацело делится на число k");
            else
                Console.WriteLine("Число a нацело не делится на число k");
        }
    }
}
