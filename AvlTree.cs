﻿using System;

namespace AvlTree
{
    public class Node
    {
        public int Key;
        public Node Left;
        public Node Right;

        public Node(int key)
        {
            Key = key;
        }
    }

    public class AvlTree
    {
        private Node root;

        // ...

        private Node RotateLeft(Node node)
        {
            var cur = node.Right;
            node.Right = cur.Left;
            cur.Left = node;
            return cur;
        }

        private Node RotateRight(Node node)
        {
            var cur = node.Left;
            node.Left = cur.Right;
            cur.Right = node;
            return cur;
        }

        private Node RotateLeftRight(Node node)
        {
            var cur = node.Left;
            node.Left = RotateLeft(cur);
            return RotateRight(node);
        }

        private Node RotateRightLeft(Node node)
        {
            var cur = node.Right;
            node.Right = RotateRight(cur);
            return RotateLeft(node);
        }

        // ...
    }

    class Program
    {
        static void Main()
        {
            // ...
        }
    }
}
