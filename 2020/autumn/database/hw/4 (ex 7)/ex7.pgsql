--ex1
SELECT producers.name, producers.surname FROM producers
    INNER JOIN movies
        ON producers.best_movie_id = movies.movie_id
    WHERE movies.year = 2000;


--ex2
SELECT producers.name, producers.surname FROM producers
    INNER JOIN producers_to_movies
        ON producers.producer_id = producers_to_movies.producer_id
    GROUP BY (producers.name, producers.surname)
    HAVING COUNT(*) > 5;


--ex3
SELECT movies.movie_id FROM movies
    INNER JOIN actors_to_movies
        ON movies.movie_id = actors_to_movies.movie_id
    GROUP BY (movies.movie_id)
    HAVING COUNT(*) > 10;


--ex4  
ALTER TABLE movies ADD COLUMN rating NUMERIC(3,1) NOT NULL DEFAULT 0.0;
ALTER TABLE movies ADD CHECK(rating >= 0 AND rating <= 10);
UPDATE movies SET rating = 9.2 WHERE movie_id = 1;
UPDATE movies SET rating = 7.8 WHERE movie_id = 2;
UPDATE movies SET rating = 8.3 WHERE movie_id = 3;
UPDATE movies SET rating = 8.8 WHERE movie_id = 4;
UPDATE movies SET rating = 7.3 WHERE movie_id = 5;
UPDATE movies SET rating = 6.9 WHERE movie_id = 6;

SELECT movies.name, movies.rating FROM movies
    WHERE movies.country = 'USA'
    ORDER BY movies.rating DESC
    LIMIT 10;


--ex5
SELECT DISTINCT movies.name FROM movies
    JOIN actors_to_movies ON movies.movie_id = actors_to_movies.movie_id
    JOIN actors ON actors_to_movies.actor_id = actors.actor_id
    JOIN movies_to_genres ON movies.movie_id = movies_to_genres.movie_id
    JOIN genres ON movies_to_genres.genre_id = genres.genre_id
    WHERE actors.motherland = 'UK' AND genres.genre_name = 'Horror';


--ex6
SELECT DISTINCT genres.genre_name FROM genres
    JOIN movies_to_genres ON genres.genre_id = movies_to_genres.genre_id
    GROUP BY (genres.genre_name)
    HAVING COUNT(*) > 1;


--ex7
-- У нас вроде нет столбца с продолжительностью фильмов ... Сделаем!
ALTER TABLE movies ADD COLUMN running_time TIME;
UPDATE movies SET running_time = '122 minutes'::interval WHERE movie_id = 1;
UPDATE movies SET running_time = '01:26:00'::interval WHERE movie_id = 2;
UPDATE movies SET running_time = '148 minutes'::interval WHERE movie_id = 3;
UPDATE movies SET running_time = '1 hour 27 minutes'::interval WHERE movie_id = 4;
UPDATE movies SET running_time = '93 minutes'::interval WHERE movie_id = 5;
UPDATE movies SET running_time = '01:12:00'::interval WHERE movie_id = 6;

SELECT * FROM movies
ORDER BY movies.running_time DESC
LIMIT 10 OFFSET 10;


--ex8
SELECT DISTINCT genres.genre_name FROM genres
    JOIN movies_to_genres ON genres.genre_id = movies_to_genres.genre_id
    JOIN movies ON movies_to_genres.movie_id = movies.movie_id
    JOIN producers_to_movies ON movies.movie_id = producers_to_movies.movie_id
    JOIN producers ON producers_to_movies.producer_id = producers.producer_id
    WHERE producers.motherland = 'UK' OR producers.motherland = 'France';