/*
UPDATE movies SET budget = 100000 WHERE movie_id = 1;
UPDATE movies SET budget = 150000 WHERE movie_id = 2;
UPDATE movies SET budget = 90000 WHERE movie_id = 3;
UPDATE movies SET budget = 143000 WHERE movie_id = 4;
UPDATE movies SET budget = 108000 WHERE movie_id = 5;
UPDATE movies SET budget = 75000 WHERE movie_id = 6;
*/


-- ex 1-5 are already done


--ex6
SELECT DISTINCT actors.* FROM actors
    JOIN actors_to_movies ON actors.actor_id = actors_to_movies.actor_id
    JOIN movies ON actors_to_movies.movie_id = movies.movie_id
    JOIN producers_to_movies ON movies.movie_id = producers_to_movies.movie_id
    JOIN producers ON producers_to_movies.producer_id = producers.producer_id
    WHERE producers.motherland = 'UK' AND movies.year >= 2007 AND movies.year <= 2010;


--ex7
SELECT
(SELECT ROUND(AVG(movies.budget)) FROM movies WHERE movies.year < 2000) AS avg_budget_before2000,
(SELECT ROUND(AVG(movies.budget)) FROM movies WHERE movies.year >= 2000 AND movies.year < 2005) AS avg_budget_from2000to2005,
(SELECT ROUND(AVG(movies.budget)) FROM movies WHERE movies.year >= 2005 AND movies.year < 2010) AS avg_budget_from2005to2010,
(SELECT ROUND(AVG(movies.budget)) FROM movies WHERE movies.year >= 2010) AS avg_budget_after2010;

/*
--ex7.1
SELECT AVG(movies.budget) FROM movies
    WHERE movies.year < 2000;
    
--ex7.2
SELECT AVG(movies.budget) FROM movies
    WHERE movies.year >= 2000 AND movies.year < 2005;

--ex7.3
SELECT AVG(movies.budget) FROM movies
    WHERE movies.year >= 2005 AND movies.year < 2010;

--ex7.4
SELECT AVG(movies.budget) FROM movies
    WHERE movies.year >= 2010;
*/


--ex8
SELECT SUM(movies.budget) FROM movies
    JOIN producers_to_movies ON movies.movie_id = producers_to_movies.movie_id
    JOIN producers ON producers_to_movies.producer_id = producers.producer_id
    WHERE producers.surname LIKE '%v' OR producers.surname LIKE '%n';


--ex9
SELECT MAX(movies.budget) AS max_budget, movies.year FROM movies
    GROUP BY(movies.year)
    ORDER BY(movies.year);


--ex10
SELECT * FROM movies
    WHERE movies.year < 2010 AND movies.budget < ALL
    (SELECT movies.budget FROM movies WHERE movies.year > 2010);


--ex11 ?
SELECT DISTINCT producers.name, producers.surname, producers.producer_id FROM producers
    JOIN producers_to_movies ON producers.producer_id = producers_to_movies.producer_id
    JOIN movies ON producers_to_movies.movie_id = movies.movie_id
    WHERE movies.budget > (SELECT MIN(movies.budget) FROM movies WHERE movies.year = 2015) AND 
          movies.budget < (SELECT MAX(movies.budget) FROM movies WHERE movies.year = 2016)
    GROUP BY(producers.name, producers.surname, producers.producer_id)
    HAVING COUNT(*) > 1;


--ex12 ?
/*
SELECT producers.name, producers.surname, producers.producer_id FROM producers
    JOIN producers_to_movies ON producers.producer_id = producers_to_movies.producer_id
    JOIN movies ON producers_to_movies.movie_id = movies.movie_id
    GROUP BY (producers.name, producers.surname, producers.producer_id) 
    HAVING COUNT(*) = 10;
    --(EXISTS (SELECT 1 WHERE movies.year < 2000 LIMIT 1);
    */

SELECT DISTINCT * FROM producers
    WHERE
        producers.producer_id IN  
        (SELECT producers.producer_id FROM producers
        JOIN producers_to_movies ON producers.producer_id = producers_to_movies.producer_id
        JOIN movies ON producers_to_movies.movie_id = movies.movie_id
        GROUP BY (producers.producer_id) 
        HAVING COUNT(*) = 10)
    OR
        producers.producer_id IN (SELECT producers.producer_id FROM producers
        JOIN producers_to_movies ON producers.producer_id = producers_to_movies.producer_id
        JOIN movies ON producers_to_movies.movie_id = movies.movie_id
        WHERE movies.year < 2000);