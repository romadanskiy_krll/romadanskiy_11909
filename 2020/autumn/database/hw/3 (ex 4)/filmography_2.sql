ALTER TABLE producer RENAME TO producers;

ALTER TABLE movies RENAME COLUMN id TO movie_id;
ALTER TABLE actors RENAME COLUMN some_column TO actor_id;
ALTER TABLE producers RENAME COLUMN some_column to producer_id;

ALTER TABLE actors ADD PRIMARY KEY (actor_id);
ALTER TABLE producers ADD PRIMARY KEY (producer_id);

CREATE TABLE actors_to_movies(
    actor_id INTEGER REFERENCES actors (actor_id) ON UPDATE CASCADE,
    movie_id INTEGER REFERENCES movies (movie_id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO actors_to_movies
VALUES
(1, 2),
(1, 3),
(2, 1),
(2, 4),
(2, 5),
(3, 6),
(4, 2),
(4, 4),
(4, 5),
(5, 1),
(5, 6),
(6, 4);

CREATE TABLE producers_to_movies(
    producer_id INTEGER REFERENCES producers (producer_id) ON UPDATE CASCADE,
    movie_id INTEGER REFERENCES movies (movie_id) ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO producers_to_movies
VALUES
(1, 2),
(1, 3),
(2, 6),
(3, 1),
(3, 4),
(4, 1),
(4, 4),
(5, 3),
(5, 5),
(6, 2);

ALTER TABLE producers ADD COLUMN best_movie_id INTEGER REFERENCES movies (movie_id) ON UPDATE CASCADE;
UPDATE producers SET best_movie_id = 2 WHERE producer_id = 1;
UPDATE producers SET best_movie_id = 6 WHERE producer_id = 2;
UPDATE producers SET best_movie_id = 4 WHERE producer_id = 3;
UPDATE producers SET best_movie_id = 4 WHERE producer_id = 4;
UPDATE producers SET best_movie_id = 5 WHERE producer_id = 5;
UPDATE producers SET best_movie_id = 2 WHERE producer_id = 6;


ALTER TABLE movies ALTER COLUMN country SET DEFAULT 'UK';

ALTER TABLE actors DROP CONSTRAINT actors_number_of_movies_check;

ALTER TABLE movies DROP CONSTRAINT movies_budget_check;
ALTER TABLE movies ADD CHECK (budget >= 1000)

ALTER TABLE movies DROP COLUMN genres

CREATE TABLE genres(
    genre_id SERIAL PRIMARY KEY NOT NULL,
    genre_name VARCHAR(30) NOT NULL
);

INSERT INTO genres (genre_name)
VALUES
('Comedy'),
('Thriller'),
('Fantasy'),
('Action'),
('Horror'),
('Animated'),
('Adventure'),
('Drama'),
('Western');

CREATE TABLE movies_to_genres(
    movie_id INTEGER REFERENCES movies (movie_id) ON UPDATE CASCADE ON DELETE CASCADE,
    genre_id INTEGER REFERENCES genres (genre_id) ON UPDATE CASCADE
);

INSERT INTO movies_to_genres
VALUES
(1, 1),
(1, 4),
(2, 5),
(3, 2),
(3, 8),
(4, 1),
(4, 6),
(5, 3),
(5, 7),
(6, 4),
(6, 8),
(6, 9);

CREATE TYPE motherland_enum as enum ('USA', 'UK', 'Russia', 'France', 'Germany');

UPDATE actors SET motherland = 'USA';
ALTER TABLE actors ALTER COLUMN motherland TYPE motherland_enum USING motherland::motherland_enum;
UPDATE actors SET motherland = 'UK' WHERE actor_id = 1;
UPDATE actors SET motherland = 'USA' WHERE actor_id = 2;
UPDATE actors SET motherland = 'Russia' WHERE actor_id = 3;
UPDATE actors SET motherland = 'USA' WHERE actor_id = 4;
UPDATE actors SET motherland = 'USA' WHERE actor_id = 5;
UPDATE actors SET motherland = 'France' WHERE actor_id = 6;

UPDATE producers SET motherland = 'USA';
ALTER TABLE producers ALTER COLUMN motherland DROP DEFAULT;
ALTER TABLE producers ALTER COLUMN motherland TYPE motherland_enum USING motherland::motherland_enum;
UPDATE producers SET motherland = 'USA' WHERE producer_id = 1;
UPDATE producers SET motherland = 'France' WHERE producer_id = 2;
UPDATE producers SET motherland = 'UK' WHERE producer_id = 3;
UPDATE producers SET motherland = 'Germany' WHERE producer_id = 4;
UPDATE producers SET motherland = 'UK' WHERE producer_id = 5;
UPDATE producers SET motherland = 'USA' WHERE producer_id = 6;

ALTER TABLE actors ADD CHECK (birthday <= NOW());
ALTER TABLE producers ADD CHECK (birthday <= NOW());

CREATE VIEW actors_on_movies_after_2000 AS
    SELECT DISTINCT actors.*
    FROM actors
    INNER JOIN actors_to_movies
    ON actors.actor_id = actors_to_movies.actor_id
    INNER JOIN movies
    ON actors_to_movies.movie_id = movies.movie_id
    WHERE movies.year > 2000;

UPDATE movies SET name = name || ' (' || year || ')';