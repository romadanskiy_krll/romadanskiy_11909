-- ex1
WITH RECURSIVE t1 AS (
    SELECT 0 AS n, 1::BIGINT AS factorial

    UNION ALL

    SELECT n + 1 AS n, (n + 1) * factorial as factorial
    FROM t1
)
SELECT * FROM t1
LIMIT(20);


--ex2
CREATE TABLE geography
(
    id SERIAL PRIMARY KEY,
    par_id INTEGER REFERENCES geography (id),
    name VARCHAR(100)
);

INSERT INTO geography
VALUES 
(1, null, 'Планета Земля'),
(2, 1, 'Континент Евразия'),
(3, 1, 'Континент Северная Америка'),
(4, 2, 'Европа'),
(5, 4, 'Россия'),
(6, 4, 'Германия'),
(7, 5, 'Москва'),
(8, 5, 'Санкт-Петербург'),
(9, 6, 'Берлин');

SELECT * FROM geography;

WITH RECURSIVE t2 AS (
    SELECT * FROM geography
    WHERE par_id = 4

    UNION ALL

    SELECT geography.id, geography.par_id, geography.name FROM geography
    JOIN t2 ON geography.par_id = t2.id
)
SELECT * FROM t2;