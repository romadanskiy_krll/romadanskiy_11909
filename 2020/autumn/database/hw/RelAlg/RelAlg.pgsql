-- 1
SELECT /*DISTINCT*/ * FROM movies
WHERE year = 2012;

-- 2
SELECT DISTINCT producers.name FROM producers
NATURAL JOIN producers_to_movies;

-- 3
SELECT DISTINCT producers.name FROM movies
JOIN producers_to_movies ON movies.movie_id = producers_to_movies.movie_id
JOIN producers ON producers_to_movies.producer_id = producers.producer_id
WHERE movies.rating > 7 AND producers.motherland = 'USA';
