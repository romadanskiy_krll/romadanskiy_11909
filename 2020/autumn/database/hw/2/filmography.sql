CREATE TABLE movies
(
    name VARCHAR(30) NOT NULL,
    description VARCHAR(30) NOT NULL,
    year INTEGER NOT NULL CHECK(year > 1900 AND year < 10 + extract(year from NOW())),
    genres VARCHAR(30) NOT NULL,
    country VARCHAR(30) NOT NULL,
    budget INTEGER CHECK(budget >= 10000) NOT NULL,   
    CONSTRAINT movies_pk PRIMARY KEY(name, year)
);

CREATE TABLE actors
(
    surname VARCHAR(30) NOT NULL,
    name VARCHAR(30) NOT NULL,
    birthday DATE NOT NULL,
    motherland VARCHAR(30) NOT NULL,
    number_of_movies INTEGER CHECK(number_of_movies > 5) NOT NULL,
    some_column SERIAL NOT NULL,
    UNIQUE (surname, name, birthday)
);

CREATE TABLE producer
(
    surname VARCHAR(30) NOT NULL,
    name VARCHAR(30) NOT NULL,
    birthday DATE NOT NULL,
    motherland VARCHAR(30) NOT NULL DEFAULT 'USA',
    some_column SERIAL NOT NULL
);

INSERT INTO movies 
VALUES 
('Film A', 'some description', 1980, 'comedy', 'Russia', 200000),
('Film B', 'some description', 1970, 'action', 'USA', 200000),
('Film C', 'some description', 1990, 'fantasy', 'Italy', 200000),
('Film D', 'some description', 2000, 'drama', 'USA', 200000),
('Film E', 'some description', 2020, 'horror', 'France', 200000),
('Film F', 'some description', 2010, 'cartoon', 'Russia', 200000);

INSERT INTO actors (surname, name, birthday, motherland, number_of_movies)
VALUES
('Ford', 'Jack', '1977-07-22', 'USA', 20),
('Holmes', 'Oliver', '1978-01-14', 'USA', 10),
('Evans', 'Mia', '1990-05-12', 'USA', 19),
('Collins', 'Leo', '1970-10-01', 'USA', 9),
('Beverly', 'Evie', '1985-06-22', 'USA', 7),
('Labert', 'Sophie', '1977-07-07', 'USA', 15);

INSERT INTO producer (surname, name, birthday, motherland)
VALUES 
('Smith', 'Alex', '1977-07-22', 'USA'),
('Miller', 'Kevin', '1978-01-14', DEFAULT),
('Wilson', 'Justin', '1990-05-12', 'Russia'),
('Brown', 'Thomas', '1970-10-01', 'France'),
('Jhnson', 'William', '1985-06-22', 'Italy'),
('Walker', 'Christopher', '1977-07-07', 'USA');

ALTER TABLE movies DROP CONSTRAINT movies_pk;

ALTER TABLE movies ADD COLUMN Id SERIAL PRIMARY KEY not null;