﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace selfHostedHttp
{
	class Program
	{
		public static void Main(string[] args) => Listen().Wait();

		private static async Task Listen()
		{
			var listener = new HttpListener();
			listener.Prefixes.Add("http://localhost:8080/");
			listener.Start();
			Console.WriteLine("Сервер начал прослушивание порта 8080");

			while (true)
			{
				var context = await listener.GetContextAsync();
				var request = context.Request;
				var response = context.Response;
				
				IPage page = new MainPage("main-page");
				switch (request.Url.AbsolutePath)
				{
					case "/":
						page = new MainPage("main-page");
						break;
					case "/about":
						page = new AboutPage("about");
						break;
					case "/album":
						page = new AlbumPage("album");
						break;
					case "/contacts":
						page = new ContactsPage("contacts");
						break;
					case "/news":
						page = new NewsPage("news");
						break;
					default:
						break;
				}
				
				var bytes = page.Content;
				response.ContentLength64 = bytes.Length;
				response.ContentType = @"text/html";

				var sw = response.OutputStream;
				sw.Write(bytes, 0, bytes.Length);
				sw.Close();
			}
			//listener.Stop();
		} 
	}
}
