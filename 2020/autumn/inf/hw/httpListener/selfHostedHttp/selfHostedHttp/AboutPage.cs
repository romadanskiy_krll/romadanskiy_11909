﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace selfHostedHttp
{
    public class AboutPage : IPage
    {
        public string HtmlName { get; }
        public byte[] Content { get; }

        public AboutPage(string htmlName)
        {
            HtmlName = htmlName;
            Content = File.ReadAllBytes($"templates/{htmlName}.html");
        }
    }
}