﻿using System.IO;

namespace selfHostedHttp
{
    public class AlbumPage : IPage
    {
        public string HtmlName { get; }
        public byte[] Content { get; }

        public AlbumPage(string htmlName)
        {
            HtmlName = htmlName;
            Content = File.ReadAllBytes($"templates/{htmlName}.html");
        }
    }
}