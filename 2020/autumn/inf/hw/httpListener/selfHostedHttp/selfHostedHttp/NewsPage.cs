﻿using System.IO;

namespace selfHostedHttp
{
    public class NewsPage : IPage
    {
        public string HtmlName { get; }
        public byte[] Content { get; }

        public NewsPage(string htmlName)
        {
            HtmlName = htmlName;
            Content = File.ReadAllBytes($"templates/{htmlName}.html");
        }
    }
}