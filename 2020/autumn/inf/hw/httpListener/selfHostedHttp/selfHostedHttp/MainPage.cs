﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace selfHostedHttp
{
    public class MainPage : IPage
    {
        public string HtmlName { get; }
        public byte[] Content { get; }

        public MainPage(string htmlName)
        {
            HtmlName = htmlName;
            Content = File.ReadAllBytes($"templates/{htmlName}.html");
        }
    }
}