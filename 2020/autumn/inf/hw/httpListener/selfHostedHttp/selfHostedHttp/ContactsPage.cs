﻿using System.IO;

namespace selfHostedHttp
{
    public class ContactsPage : IPage
    {
        public string HtmlName { get; }
        public byte[] Content { get; }

        public ContactsPage(string htmlName)
        {
            HtmlName = htmlName;
            Content = File.ReadAllBytes($"templates/{htmlName}.html");
        }
    }
}