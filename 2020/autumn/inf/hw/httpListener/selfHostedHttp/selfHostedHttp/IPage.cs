﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace selfHostedHttp
{
    public interface IPage
    {
        string HtmlName { get; }
        byte[] Content { get; }
    }
}