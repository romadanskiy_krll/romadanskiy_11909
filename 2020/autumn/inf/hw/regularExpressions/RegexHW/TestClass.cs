﻿using System;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace RegexHW
{
    [TestFixture]
    
    // 01a
    
    public class TestClass
    {
        [TestCase("1")]
        [TestCase("0.3")]
        [TestCase("0,3")]
        [TestCase("-12.6")]
        [TestCase("+2")]
        [TestCase("0.(1)")]
        [TestCase("0.0(1)")]
        [TestCase("0.064")]
        [TestCase("0.0(64)")]
        [TestCase("0.(064)")]
        [TestCase("-28605,0004507(03)")]
        public void Ex_01a_PatternMatchTest(string str)
        {
            Assert.True(Regex.IsMatch(str, Ex_01a.Pattern));
        }

        [TestCase("1.")]
        [TestCase("1.0")]
        [TestCase("1.(0)")]
        [TestCase("1.()")]
        [TestCase("0.3(0)")]
        [TestCase("001")]
        [TestCase("-3.750")]
        public void Ex_01a_PatternNoMatchTest(string str)
        {
            Assert.False(Regex.IsMatch(str, Ex_01a.Pattern));
        }
        
        // 02c

        [TestCase("0")]
        [TestCase("1")]
        [TestCase("000")]
        [TestCase("111")]
        [TestCase("1010")]
        [TestCase("0101")]
        [TestCase("101")]
        [TestCase("010")]
        public void Ex_02c_PatternMatchTest(string str)
        {
            Assert.True(Regex.IsMatch(str, Ex_02c.Pattern));
        }

        [TestCase("2")]
        [TestCase("001")]
        [TestCase("100")]
        [TestCase("1001")]
        [TestCase("0110")]
        [TestCase("11011")]
        [TestCase("00100")]
        public void Ex_02c_PatternNoMatchTest(string str)
        {
            Assert.False(Regex.IsMatch(str, Ex_02c.Pattern));
        }

        [TestCase(new [] {"1", "0", "2"}, new [] {1, 2})]
        [TestCase(new [] {"10", "00", "11", "01"}, new [] {1, 2, 3, 4})]
        [TestCase(new [] {"1010", "1001", "0101", "0110"}, new [] {1, 3})]
        [TestCase(new [] {"001", "101", "010"}, new [] {2, 3})]
        [TestCase(new [] {"10100", "01010"}, new [] {2})]
        [TestCase(new [] {"0000", "11111", "00100"}, new [] {1, 2})]
        public void Ex_02c_DoTest(string[] arr, int[] expectedArr)
        {
            Assert.AreEqual(expectedArr, Ex_02c.Do(arr));
        }
        
        // 03c
        
        [TestCase(0)]
        [TestCase(52)]
        [TestCase(777774)]
        [TestCase(596)]
        [TestCase(258)]
        public void Ex_03c_PatternMatchTest(int number)
        {
            Assert.True(Regex.IsMatch(number.ToString(), Ex_03c.Pattern));
        }
        
        [TestCase(25)]
        [TestCase(-2)]
        [TestCase(77777)]
        public void Ex_03c_PatternNoMatchTest(int number)
        {
            Assert.False(Regex.IsMatch(number.ToString(), Ex_03c.Pattern));
        }

        [Test]
        public void Ex_03c_ResultArrayLengthIs10Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_03c();
                var resArr = ex.Do();
                Assert.True(resArr.Length == 10);
            }
        }

        [Test]
        public void Ex_03c_CountMoreThen9Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_03c();
                var resArr = ex.Do();
                Assert.True(ex.Count >= 10);
            }
        }

        [Test]
        public void Ex_03c_EvenNumbersOnResultTest()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_03c();
                var resArr = ex.Do();
                foreach (var e in resArr)
                {
                    Assert.True(e % 2 == 0);
                }
            }
        }
        
        // 03b
        
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(12)]
        [TestCase(122)]
        [TestCase(122)]
        [TestCase(122122)]
        [TestCase(211)]
        [TestCase(777777)]
        [TestCase(22122)]
        [TestCase(18652094)]
        public void Ex_03b_PatternMatchTest(int number)
        {
            Assert.True(Regex.IsMatch(number.ToString(), Ex_03b.Pattern));
        }
        
        [TestCase(-39)]
        [TestCase(222)]
        [TestCase(12221)]
        [TestCase(1121212221)]
        [TestCase(502493)]
        [TestCase(200)]
        public void Ex_03b_PatternNoMatchTest(int number)
        {
            Assert.False(Regex.IsMatch(number.ToString(), Ex_03b.Pattern));
        }

        [Test]
        public void Ex_03b_ResultArrayLengthIs10Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_03b();
                var resArr = ex.Do();
                Assert.True(resArr.Length == 10);
            }
        }

        [Test]
        public void Ex_03b_CountMoreThen9Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_03b();
                var resArr = ex.Do();
                Assert.True(ex.Count >= 10);
            }
        }

        [Test]
        public void Ex_03b_NoThreeEvenNumbersInARowOnResultTest()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_03b();
                var resArr = ex.Do();
                for (var j = 0; i < resArr.Length; i++)
                {
                    var number = resArr[j];
                    var even = 0;
                    while (number != 0)
                    {
                        var n = number % 10;
                        even = (n % 2 == 0) ? even + 1 : 0;
                        if (even >= 3) Assert.Fail();
                        number /= 10;
                    }
                }
            }
        }
        
        // 04b

        [TestCase(2222)]
        [TestCase(22222)]
        [TestCase(2468)]
        [TestCase(24680)]
        [TestCase(80000)]
        public void Ex_04b_PatternMatchTest(int number)
        {
            Assert.True(Regex.IsMatch(number.ToString(), Ex_04b.Pattern));
        }
        
        [TestCase(246)]
        [TestCase(246802)]
        [TestCase(2225)]
        [TestCase(13579)]
        public void Ex_04b_PatternNoMatchTest(int number)
        {
            Assert.False(Regex.IsMatch(number.ToString(), Ex_04b.Pattern));
        }

        [Test]
        public void Ex_04b_ResultArrayLengthIs10Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_04b();
                var resArr = ex.Do();
                Assert.True(resArr.Length == 10);
            }
        }

        [Test]
        public void Ex_04b_CountMoreThen9Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_04b();
                var resArr = ex.Do();
                Assert.True(ex.Count >= 10);
            }
        }

        [Test]
        public void Ex_04b_4To6EvenNumbersOnResultTest()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_04b();
                var resArr = ex.Do();
                for (var j = 0; i < resArr.Length; i++)
                {
                    var number = resArr[j];
                    var c = 0;
                    while (number != 0)
                    {
                        var n = number % 10;
                        if (n % 2 != 0)
                            Assert.Fail();
                        else
                            c++;
                        number /= 10;
                    }
                    if (c < 4 || c > 5) 
                        Assert.Fail();
                }
            }
        }
        
        // 05b
        
        [TestCase(1221221)]
        [TestCase(22122)]
        [TestCase(2020)]
        [TestCase(2000)]
        [TestCase(702702)]
        [TestCase(838388388)]
        public void Ex_05b_PatternMatchTest(int number)
        {
            Assert.True(Regex.IsMatch(number.ToString(), Ex_05b.Pattern));
        }
        
        [TestCase(122121)]
        [TestCase(2255255)]
        [TestCase(66636)]
        [TestCase(77000)]
        public void Ex_05b_PatternNoMatchTest(int number)
        {
            Assert.False(Regex.IsMatch(number.ToString(), Ex_05b.Pattern));
        }

        [Test]
        public void Ex_05b_ResultArrayLengthIs10Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_05b();
                var resArr = ex.Do();
                Assert.True(resArr.Length == 10);
            }
        }

        [Test]
        public void Ex_05b_CountMoreThen9Test()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_05b();
                var resArr = ex.Do();
                Assert.True(ex.Count >= 10);
            }
        }

        [Test]
        public void Ex_05b_2OrMorePairsOfEvenNumbersOnResultTest()
        {
            for (var i = 0; i < 15; i++)
            {
                var ex = new Ex_05b();
                var resArr = ex.Do();
                for (var j = 0; i < resArr.Length; i++)
                {
                    var number = resArr[j];
                    var evenCount = 0;
                    var pairsCount = 0;
                    while (number != 0)
                    {
                        var n = number % 10;
                        evenCount = (n % 2 == 0) ? evenCount + 1 : 0;
                        if (evenCount == 2)
                        {
                            pairsCount++;
                            evenCount = 0;
                        }
                        number /= 10;
                    }
                    if (pairsCount < 2) Assert.Fail();
                }
            }
        }
    }
}