﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace RegexHW
{
    public class Ex_03b
    {
        public const string Pattern = @"^((([13579]|[24680][13579])*[24680]{2}[13579])*([13579]|[24680][13579])*[24680]{2}" +
                                      @"|([13579]*[24680]([24680][13579]|[13579]))*[13579]*[24680]" +
                                      @"|(([13579]*|[24680]([24680][13579]|[13579])))*)$";

        public int Count { get; private set; }

        public int[] Do()
        {
            var reg = new Regex(Pattern);
            var rnd = new Random();
            var res = new List<int>();
            while (res.Count != 10)
            {
                var n = rnd.Next();
                if (reg.IsMatch(n.ToString()))
                {
                    res.Add(n);
                }
                Count++;
            }

            return res.ToArray();
        }
    }
}