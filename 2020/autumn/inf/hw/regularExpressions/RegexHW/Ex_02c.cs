﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace RegexHW
{
    public class Ex_02c
    {
        public const string Pattern = "^(0+|1+|(10)+|(01)+|(10)+1|(01)+0)$";
        
        public static int[] Do(string[] arr)
        {
            var res = new List<int>();
            var reg = new Regex(Pattern);
            for (var i = 0; i < arr.Length; i++)
            {
                if (reg.IsMatch(arr[i])) 
                    res.Add(i+1);
            }
            
            return res.ToArray();
        }
    }
}