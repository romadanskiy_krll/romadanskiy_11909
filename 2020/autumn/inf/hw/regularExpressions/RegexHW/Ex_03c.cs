﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace RegexHW
{
    public class Ex_03c
    {
        public const string Pattern = @"^([1-9]\d*[24680]|0)$";

        public int Count { get; private set; }

        public int[] Do()
        {
            var reg = new Regex(Pattern);
            var rnd = new Random();
            var res = new List<int>();
            while (res.Count != 10)
            {
                var n = rnd.Next();
                if (reg.IsMatch(n.ToString()))
                {
                    res.Add(n);
                }
                Count++;
            }

            return res.ToArray();
        }
    }
}