window.onload = function(){

	var res = document.getElementById('res');

	var numbers = document.getElementsByClassName('btn');

	for (let btn of numbers) {
		btn.addEventListener("click", () => {
			if (btn.innerHTML == 'c'){
				res.innerHTML = '0';
			} else if (btn.innerHTML == '='){
				res.innerHTML = eval(res.innerHTML);
			} else if (res.innerHTML == '0') {
				res.innerHTML = btn.innerHTML;
			} else {
				res.innerHTML += btn.innerHTML;
			}
		})
	}
}