const table = document.querySelector('table');
const form = document.getElementsByClassName('form-add')[0];
const search = document.getElementsByClassName('form-search')[0];
const inputs = document.querySelectorAll('input[data-rule]');

const allInputs = document.querySelectorAll('input[type="text"]');
for (let inp of allInputs){
    inp.value = "";
}

search.addEventListener('keyup', (e) => {
    let filter = document.getElementsByClassName('filter')[0].value;
    let phrase = document.getElementsByClassName('search-text')[0].value;
    
    let cellIndex = 0;
    for (let i = 0; i < table.rows[0].cells.length; i++){
        let flagInd = false;
        if (table.rows[0].cells[i].innerHTML === filter) {
            cellIndex = i;
        }
        if (flagInd) break;
    }

    let flag = false;
    for (var i = 1; i < table.rows.length; i++) {
        flag = false;
        flag = table.rows[i].cells[cellIndex].innerHTML.startsWith(phrase);
        if (flag) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }

    }

});

function validate () {
    let rule = this.dataset.rule;
    let value = this.value;
    let check;

    switch (rule) {
        case 'number':
            check = /^\d+$/.test(value);
            break;
        case 'name':
            check = /^[А-Я]{1}[а-яё]{1,23}$/.test(value);
            break;
        case 'date':
            check = /^(0[1-9]|[12][0-9]|3[01])[.](0[1-9]|1[012])[.](19|20)\d\d$/.test(value);
            break;
        case 'group':
            check = /^(\d|[1-9]\d|[1-9]\d\d)[-](\d|[1-9]\d|[1-9]\d\d)$/.test(value);
            break;
        case 'email':
            check = /^[a-z0-9_-]{1,30}@[a-z]{2,10}\.[a-z]{2,6}$/.test(value);
    }

    this.classList.remove('valid');
    this.classList.remove('invalid');
    if (check) {
        this.classList.add('valid');
    } else {
        this.classList.add('invalid');
    }
}

for (let input of inputs) {
    input.addEventListener('keyup', validate);
    input.addEventListener('blur', validate);
}

form.addEventListener('submit', (e) => {
    let check = true;
    for (let inp of inputs){
        if (!inp.classList.contains('valid')){
            check = false;
            break;
        }
    }
    if (!check) {
        alert ('Заполните все поля!');
        return
    }
    
    for (var i = 1; i < table.rows.length; i++) {
        if (table.rows[i].cells[0].innerHTML == inputs[0].value) {
            alert ('Студент с таким номером уже есть!');
            return;
        }
    }

    let tr = document.createElement('TR');
    for(let i=0; i < 7; i++){
        let td = document.createElement('TD');
        td.innerHTML = form.elements[i].value;
        tr.appendChild(td);
    }  
    const tbody = table.querySelector('tbody'); 
    tbody.appendChild(tr);

    for(let inp of inputs){
        inp.value = '';
        inp.classList.remove('invalid');
        inp.classList.remove('valid');
    }
});

table.addEventListener('click', (e) => {
    const tb = e.target;
    if (tb.nodeName !== 'TH') return;
    const index = tb.cellIndex;
    const type = tb.getAttribute('type');
    sortTable(index, type);
});

function sortTable (index, type) {

    function compare (row1, row2) {
        const data1 = row1.cells[index].innerHTML;
        const data2 = row2.cells[index].innerHTML;
    
        switch (type) {
            case 'number':
                return +data1 - +data2;
                break;
            case 'text':
                if (data1 > data2) return 1;
                if (data1 < data2) return -1;
                return 0;
                break;
            case 'date':
                const date1 = new Date(data1.split('.').reverse().join('-'));
                const date2 = new Date(data2.split('.').reverse().join('-'));
                return date1.getTime() - date2.getTime();
                break;
            case 'group':
                const group1 = data1.split('-');
                const group2 = data2.split('-');
                if (+group1[0] < +group2[0]) return -1;
                if (+group1[0] > +group2[0]) return 1;
                return +group1[1] - +group2[1];
                break;
            case 'email':
                const email1 = data1.split('@');
                const email2 = data2.split('@');
                if (email1[0] < email2[0]) return -1;
                else if (email1[0] > email2[0]) return 1;
                else {
                    if (email1[1] < email2[1]) return -1;
                    if (email1[1] > email2[1]) return 1;
                    return 0;
                }
                break;
        }
    }

    const tbody = table.querySelector('tbody');
    let rows = Array.prototype.slice.call(tbody.rows);
    rows.sort(compare);
    table.removeChild(tbody);
    for (let i=0; i < rows.length; i++) {
        tbody.appendChild(rows[i]);
    }
    table.appendChild(tbody);
}

