﻿namespace Lesson3
{
    public interface ILogger
    {
        public void Log(string info);
    }
}