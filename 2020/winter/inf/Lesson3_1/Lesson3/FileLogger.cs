﻿using System;
using System.IO;

namespace Lesson3
{
    public class FileLogger : ILogger
    {
        public void Log(string info)
        {
            using var sw = new StreamWriter("Log.txt", true);
            sw.WriteLine($"logged info: {info}");
        }
    }
}