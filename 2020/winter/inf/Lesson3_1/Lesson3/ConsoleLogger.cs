﻿using System;

namespace Lesson3
{
    public class ConsoleLogger : ILogger
    {
        public void Log(string info)
        {
            Console.WriteLine($"logged info: {info}");
        }
    }
}