﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Text;

namespace Lesson1
{
    public class CounterMiddleware
    {
        private readonly string path = "Files/Counter.txt";
        private readonly RequestDelegate _next;

        public CounterMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (context.Request.Query.ContainsKey("name"))
            {
                var count = await File.ReadAllTextAsync(path);
                var n = int.Parse(count) + 1;
                count = n.ToString();
                await File.WriteAllTextAsync(path, count);
                /*
                await using var fs = new FileStream("Files/Counter.txt", FileMode.Open);
                var bytes = new byte[fs.Length];
                await fs.ReadAsync(bytes, 0, bytes.Length);
                var count = Encoding.UTF8.GetString(bytes);
                var arr = count.ToCharArray();
                var n = Convert.ToInt32(count);
                ++n;
                bytes = Encoding.UTF8.GetBytes(n.ToString());
                await fs.WriteAsync(bytes, 0, bytes.Length);
                */
                context.Response.Headers["count"] = count;
            }
            
            await _next(context);
        }
    }
}