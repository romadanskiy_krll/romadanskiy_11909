using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Lesson1
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCounter(); // счётчик запросов
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    if (context.Request.Query.ContainsKey("name"))
                        await context.Response.WriteAsync($"Hello {context.Request.Query["name"]}! You are the {context.Response.Headers["count"]}.");
                    else
                    {
                        var response = context.Response;
                        var bytes = await File.ReadAllBytesAsync("Pages/Home.html");
                        response.ContentLength = bytes.Length;
                        response.ContentType = @"text/html";
                        await using var sw = response.Body;
                        await sw.WriteAsync(bytes, 0, bytes.Length);
                    }
                });
            });
        }
    }
}
