﻿using Microsoft.AspNetCore.Builder;

namespace Lesson1
{
    public static class AppExtensions
    {
        public static void UseCounter(this IApplicationBuilder builder)
        {
            builder.UseMiddleware<CounterMiddleware>();
        }
    }
}