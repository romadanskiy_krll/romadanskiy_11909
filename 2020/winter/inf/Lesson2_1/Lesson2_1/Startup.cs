using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Lesson2_1
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            services.AddSession();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseSession();

            app.Map("/signup", HandleEnter);
            app.Map("/account", HandleVip);
            app.Map("/message", HandleVip);
            app.Map("/music", HandleVip);
            app.Map("/signout", HandleExit);
            
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    if (context.Session.Keys.Contains("status"))
                        context.Response.Redirect("/account");
                    else
                        await LoadPage(context);
                });
                endpoints.MapGet("/noaccess", async context =>
                {
                    await LoadPage(context);
                });
                endpoints.MapGet("/settings", async context =>
                {
                    await LoadPage(context);
                });
            });

            app.Run(async context =>
            {
                await context.Response.WriteAsync("Page Not Found");
            });
        }
        
        private static void HandleEnter(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                if (!context.Session.Keys.Contains("status"))
                    context.Session.SetString("status", "vip");
                context.Response.Redirect("/account");
            });
        }
        
        private static void HandleVip(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                if (!context.Session.Keys.Contains("status"))
                    context.Response.Redirect("/noaccess");
                await LoadPage(context);
            });
        }
        
        private static void HandleExit(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                if (context.Session.Keys.Contains("status"))
                    context.Session.Remove("status");
                context.Response.Redirect("/");
            });
        }

        private static async Task LoadPage(HttpContext context)
        {
            var path = new Uri(context.Request.GetDisplayUrl()).AbsolutePath;
            if (path == "/")
                path = "/main";
            var response = context.Response;
            var bytes = await File.ReadAllBytesAsync($"Pages{path}.html");
            response.ContentLength = bytes.Length;
            response.ContentType = @"text/html";
            await using var sw = response.Body;
            await sw.WriteAsync(bytes, 0, bytes.Length);
        }
    }
}