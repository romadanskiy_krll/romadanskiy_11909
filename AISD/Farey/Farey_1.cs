﻿using System;

namespace Farei_1
{
    class Program
    {
        static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            F(n);
        }

        public static void F(int n)
        {
            Console.WriteLine("{0}/{1}", 0, 1); // 0/1
            FindMediant(0, 1, 1, 1, n);
            Console.WriteLine("{0}/{1}", 1, 1); // 1/1
        }

        public static void FindMediant(int a, int b, int c, int d, int n)
        {
            // a/b , p/q = (a+b)/(c+d) , c/d
            var p = a + c;
            var q = b + d; 
            if (q <= n)
            {
                FindMediant(a, b, p, q, n);
                Console.WriteLine("{0}/{1}", p, q);
                FindMediant(p, q, c, d, n);
            }
        }
    }
}
