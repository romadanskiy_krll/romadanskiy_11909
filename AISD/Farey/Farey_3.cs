﻿using System;

namespace Farei_3
{
    class Program
    {
        public class Fraction
        {
            public int numerator;
            public int denominator;
            public Fraction next;
            public Fraction(int numerator, int denominator, Fraction next)
            {
                this.numerator = numerator;
                this.denominator = denominator;
                this.next = next;
            }
            public override string ToString()
            {
                return string.Format("{0}/{1}", numerator, denominator);
            }
        }

        public static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            var fraction = F(n);
            while (fraction != null)
            {
                Console.WriteLine(fraction.ToString());
                fraction = fraction.next;
            }
        }

        public static Fraction F(int n)
        {
            var f2 = new Fraction(1, 1, null);
            var f1 = new Fraction(0, 1, f2);
            for (int i = 2; i <= n; i++)
            {
                var currentF = f1;
                while (currentF.next != null)
                {
                    if (currentF.denominator + currentF.next.denominator == i)
                    {
                        var newF = new Fraction(currentF.numerator + currentF.next.numerator, i, currentF.next);
                        currentF.next = newF;
                    }
                    currentF = currentF.next;
                }
            }

            return f1;
        }
    }
}
