﻿using System;

namespace Farei_2
{
    class Program
    {
        public static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            F(n);
        }

        public static void F(int n)
        {
            // a/b , c/d , (c-a)/(d-b)
            var a = 0;
            var b = 1;
            var c = 1;
            var d = n;
            Console.WriteLine("{0}/{1}", a, b); // 0/1
            while(c <= n)
            {
                var k = (int)((n + b) / d);
                var a1 = a;
                var b1 = b;
                a = c;
                b = d;
                c = k * c - a1;
                d = k * d - b1;
                Console.WriteLine("{0}/{1}", a, b);
            }
        }
    }
}
