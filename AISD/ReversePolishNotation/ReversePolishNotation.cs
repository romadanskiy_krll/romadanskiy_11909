﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ReversePolishNotation
{
    class Program
    {
        public static void Main()
        {
			var str = Console.ReadLine();
			var result = Calculation(str);
			Console.WriteLine(result);
        }

		public static bool IsNum(string element) => double.TryParse(element, out _);

		public static double Calculation(string str)
		{
			var operations = new Dictionary<string, Func<double, double, double>>
			{
				{ "+", (y, x) => x + y },
				{ "-", (y, x) => x - y },
				{ "*", (y, x) => x * y },
				{ "/", (y, x) => x / y }
			};

			var elements = str.Trim().Split(' ');
			var stack = new Stack<double>();
			foreach (var e in elements)
			{
				if (IsNum(e))
					stack.Push(double.Parse(e));
				else if (operations.ContainsKey(e))
					stack.Push(operations[e](stack.Pop(), stack.Pop()));
				else
					throw new ArgumentException();
			}

			return stack.Pop();
		}
	}
}
