﻿using System;
using System.Collections.Generic;

namespace LexicSort
{
    class Program
    {
        public class QueueItem
        {
            public string Value;
            public QueueItem Next;
        }

        public class MyQueue
        {
            private QueueItem head;
            private QueueItem tail;
            private int count;

            public void Enqueue(string value)
            {
                if (head == null)
                    tail = head = new QueueItem { Value = value, Next = null };
                else
                {
                    var item = new QueueItem { Value = value, Next = null };
                    tail.Next = item;
                    tail = item;
                }
                count++;
            }

            public string Dequeue()
            {
                if (head == null) throw new InvalidOperationException();
                var result = head.Value;
                head = head.Next;
                if (head == null) tail = null;
                count--;

                return result;
            }

            public int Count { get { return count; } }

            public string Last { get { return tail.Value; } }

            public void CombineWith(MyQueue queue)
            {
                if (this.head == null)
                {
                    this.head = queue.head;
                    this.tail = queue.tail;
                }
                else
                {
                    this.tail.Next = queue.head;
                    this.tail = queue.tail;
                }
                this.count += queue.Count;
            }

            public void LexicSort(string alph)
            {
                var n = this.Last.Length;
                var m = this.Count;
                for (var i = n - 1; i >= 0; i--)
                {
                    var dict = MakeDictionary(alph);
                    for (var j = 0; j < m; j++)
                    {
                        var current = this.Dequeue();
                        var key = current[i];
                        dict[key].Enqueue(current);
                    }

                    foreach (var key in dict.Keys)
                    {
                        if (dict[key].Count != 0)
                            this.CombineWith(dict[key]);
                    }
                }
            }

        }

        public static void Main()
        {
            Headline();
            var data = DataInput();
            var sequence = data.Item1;
            var alph = data.Item2;
            sequence.LexicSort(alph);
            DataOutput(sequence);
        }

        public static Dictionary<char, MyQueue> MakeDictionary(string alph)
        {
            var dict = new Dictionary<char, MyQueue>();
            foreach (var e in alph)
                dict.Add(e, new MyQueue());

            return dict;
        }

        public static void Headline()
        {
            Console.WriteLine(@" / / / Лексикографическая сортировка последовательностей одинаковой длины \ \ \ ");
            Console.WriteLine();
        }

        public static Tuple<MyQueue, string> DataInput()
        {
            Console.Write("Количество строк в последовательности: ");
            var n = int.Parse(Console.ReadLine());

            var sequence = new MyQueue();
            for (var i = 0; i < n; i++)
            {
                sequence.Enqueue(Console.ReadLine());
            }
            Console.WriteLine();

            Console.Write("Алфавит: ");
            var alph = Console.ReadLine();
            
            return new Tuple<MyQueue, string>(sequence, alph);
        }

        private static void DataOutput(MyQueue sequence)
        {
            Console.WriteLine("\r\nРезультат:");
            while (sequence.Count != 0)
                Console.WriteLine(sequence.Dequeue());
        }

    }
}
