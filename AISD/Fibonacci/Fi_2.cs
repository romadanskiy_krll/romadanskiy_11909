﻿using System;

namespace Fi_2
{
    class Program
    {
        static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            if (n < 0) throw new Exception("Введено отрицательное число");
            Console.WriteLine(F(n));
        }

        public static long F(int n)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;
            long a = 0;
            long b = 1;
            long c = 1;
            for (var i = 2; i <= n; i++)
            {
                c = a + b;
                a = b;
                b = c;
            }

            return c;
        }
    }
}
