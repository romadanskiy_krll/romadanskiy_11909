﻿using System;

namespace Fi_3
{
    class Program
    {
        private static readonly long[,] mtx = { { 1, 1 }, 
                                                { 1, 0 } };
        private static readonly long[,] identity = { { 1, 0 },
                                                     { 0, 1 } };

        public static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            if (n < 0) throw new Exception("Введено отрицательное число");
            Console.WriteLine(F(n));
        }

        public static long F(int n)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;

            return Power(mtx, n)[0, 1];
        }

        public static long[,] Power(long[,] matrix, int n)
        {
            if (n == 0) return identity;
            if (n == 1) return mtx;
            if (n % 2 == 0)
            {
                var m = Power(matrix, n / 2);
                return Multiply2x2(m, m);
            }
            else
            {
                var m = Power(matrix, n - 1);
                return Multiply2x2(matrix, m);
            }
        }

        public static long[,] Multiply2x2(long[,] m1, long[,] m2)
        {
            /*
            var a11 = m1[0,0] * m2[0,0] + m1[0,1] * m2[1,0];
            var a12 = m1[0,0] * m2[0,1] + m1[0,1] * m2[1,1];
            var a21 = m1[1,0] * m2[0,0] + m1[1,1] * m2[1,0];
            var a22 = m1[1,0] * m2[0,1] + m1[1,1] * m2[1,1];

            return new[,] { { a11, a12 },
                            { a21, a22 } };
            */

            var result = new long[2, 2];
            for (int i = 0; i < 2; i++)
                for (int j = 0; j < 2; j++)               
                    for (int k = 0; k < 2; k++)                   
                        result[i, j] += m1[i, k] * m2[k, j];

            return result;
        }
    }
}
