﻿using System;

namespace Fi_1
{
    class Program
    {
        static void Main()
        {
            var n = int.Parse(Console.ReadLine());
            if (n < 0) throw new Exception("Введено отрицательное число");
            Console.WriteLine(F(n));
        }

        public static long F(int n)
        {
            if (n == 0) return 0;
            if (n == 1) return 1;

            return F(n - 2) + F(n - 1);
        }
    }
}
