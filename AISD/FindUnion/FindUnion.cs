﻿using System;
using System.Collections.Generic;

namespace FindUnion
{
    public class Pair
    {
        public readonly int A;
        public readonly int B;

        public Pair(int a, int b)
        {
            A = a;
            B = b;
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}", A, B);
        }
    }

    class Program
    {
        public static void Main()
        {
            HeadLine();
            var data = DataInput();
            var n = data.Item1;
            var count = data.Item2;
            var array = MakeDefaultArray(n);
            var result = GetResult(count, array);
            DataOutput(result);
        }

        private static List<Pair> GetResult(int count, int[] array)
        {
            var result = new List<Pair>();
            for (var i = 0; i < count; i++)
            {
                var pair = GetNewPair();
                if (pair.A > array.Length || pair.B > array.Length)
                    throw new Exception(string.Format("Число > {0}", array.Length));
                if (Find(pair.A, array) != Find(pair.B, array))
                {
                    Union(pair, array);
                    result.Add(pair);
                }
            }

            return result;
        }

        public static void HeadLine()
        {
            Console.WriteLine(@" / / / Задача ""Объединить-Найти"" \ \ \ ");
            Console.WriteLine();
        }

        public static Tuple<int, int> DataInput()
        {
            Console.Write("Длина массива: ");
            var n = int.Parse(Console.ReadLine());
            Console.Write("Количество пар: ");
            var count = int.Parse(Console.ReadLine());

            return new Tuple<int, int>(n, count);
        }

        public static void DataOutput(List<Pair> result)
        {
            Console.WriteLine("\nРезультат:");
            foreach (var pair in result)
                Console.WriteLine(pair);
        }

        public static int[] MakeDefaultArray(int n)
        {
            var array = new int[n];
            for (var i = 0; i < n; i++)
                array[i] = i + 1;

            return array;
        }

        public static Pair GetNewPair()
        {
            var pair = Console.ReadLine().Split('-');
            return new Pair(int.Parse(pair[0]), int.Parse(pair[1]));
        }

        public static int Find(int number, int[] array)
        {
            return array[number - 1];
        }

        public static void Union(Pair pair, int[] array)
        {
            for(var i = 0; i < array.Length; i++)
            {
                if (array[i] == array[pair.A - 1])
                    array[i] = array[pair.B - 1];
            }
        }

    }
}
