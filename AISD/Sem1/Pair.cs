﻿using System;

namespace Sem1
{
    /// <summary>
    /// Пара слов Слово-Перевод
    /// </summary>
    public class Pair
    {
        public string Word;
        public string Translate;

        /// <summary>
        /// Создаёт новую пару слов
        /// </summary>
        /// <param name="word">Слово на исходном языке</param>
        /// <param name="translate">Перевод</param>
        public Pair(string word, string translate)
        {
            Word = word;
            Translate = translate;
        }

        public override string ToString()
        {
            return string.Format("{0}-{1}", Word, Translate);
        }
    }
}
