﻿using System;

namespace Sem1
{
    /// <summary>
    /// Узел односвязного списка MyQueue
    /// </summary>
    public class MyQueueItem
    {
        public Pair Value { get; set; }
        public MyQueueItem Next { get; set; }
    }
}
