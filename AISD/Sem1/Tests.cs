﻿using System;
using NUnit.Framework;

namespace Sem1
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void PairToStringTest()
        {
            var pair1 = new Pair("one", "один");
            var pair2 = new Pair("two", "два");
            Assert.AreEqual("one-один", pair1.ToString());
            Assert.AreEqual("two-два", pair2.ToString());
        }

        [Test]
        public void MyQueueAddTest()
        {
            var queue = new MyQueue();
            var last = new Pair("ha", "ha");
            queue.Add(new Pair("one", "один"));
            foreach (var pair in queue)
                last = pair;
            Assert.AreEqual("one-один", last.ToString());
            queue.Add(new Pair("two", "два"));
            foreach (var pair in queue)
                last = pair;
            Assert.AreEqual("two-два", last.ToString());
        }

        [Test]
        public void MyQueueCount1Test()
        {
            var queue = new MyQueue();
            queue.Add(new Pair("one", "один"));
            Assert.AreEqual(1, queue.Count1);
            queue.Add(new Pair("two", "два"));
            Assert.AreEqual(2, queue.Count1);
            queue.Add(new Pair("four", "четыре"));
            Assert.AreEqual(2, queue.Count1);
            queue.Remove("one");
            Assert.AreEqual(1, queue.Count1);
        }

        [Test]
        public void MyQueueDequeueTest()
        {
            var queue = new MyQueue();
            queue.Add(new Pair("one", "один"));
            queue.Add(new Pair("two", "два"));
            var removedPair = queue.Dequeue();
            Assert.AreEqual("one-один", removedPair.ToString());
            foreach (var pair in queue)
                if (pair.ToString() == "one-один") Assert.Fail();
        }

        [Test]
        public void MyQueueRemoveTest()
        {
            var queue = new MyQueue();
            queue.Add(new Pair("a", "a"));
            queue.Add(new Pair("b", "b"));
            queue.Add(new Pair("c", "c"));
            queue.Remove("a");
            foreach (var pair in queue)
                if (pair.Word == "a") Assert.Fail();
            queue.Remove("c");
            foreach (var pair in queue)
                if (pair.Word == "c") Assert.Fail();
        }

        [Test]
        public void MyQueueCreateUniqueQueueTest()
        {
            var queue = new MyQueue();
            queue.Add(new Pair("a", "a"));
            queue.Add(new Pair("b", "b"));
            queue.Add(new Pair("c", "abc"));
            queue.Add(new Pair("d", "abc"));
            queue.Add(new Pair("e", "f"));
            queue.Add(new Pair("f", "abc"));
            var uniqueQueue = queue.CreateUniqueQueue();
            foreach (var pair in uniqueQueue)
                if (pair.Translate == "abc") Assert.Fail();
        }

        [Test]
        public void MyQueueFindTranslateTest()
        {
            var queue = new MyQueue();
            queue.Add(new Pair("one", "один"));
            queue.Add(new Pair("two", "два"));
            var translate1 = queue.FindTranslate("one");
            var translate2 = queue.FindTranslate("two");
            Assert.AreEqual("один", translate1);
            Assert.AreEqual("два", translate2);
        }
    }
}
