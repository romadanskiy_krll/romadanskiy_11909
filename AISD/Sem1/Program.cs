﻿using System;
using System.IO;

namespace Sem1
{
    class Program
    {
        public static void Main()
        {
            var queue = Coding("dictionary.txt");
            ResultOfMethod(queue, "Получен список слов:");

            var n = queue.Count1;
            Console.WriteLine(
                string.Format("Число элементов списка, которые при переводе изменяют длину слова не более чем на единицу: {0}\r\n", n));

            var newPair = new Pair("home", "дом");
            queue.Add(newPair);
            ResultOfMethod(queue, string.Format("Добавлена новая пара слов {0}:", newPair));

            var word = "hello";
            queue.Remove(word);
            ResultOfMethod(queue, string.Format("Удалено слово {0}:", word));

            var uniqueQueue = queue.CreateUniqueQueue();
            ResultOfMethod(uniqueQueue, "Получен список слов с уникальным переводом:");

            var path = "text.txt";
            Console.WriteLine(string.Format("Перевод текстового файла \"{0}\":", path));
            Translate(path, uniqueQueue, Console.Write);

            Decoding(uniqueQueue, "dictionaryResult.txt");
        }

        /// <summary>
        /// Выводит содержимое списка на экран
        /// </summary>
        /// <param name="queue">Список, над которым выполнялась операция</param>
        /// <param name="headLine">Описание выполненной операции</param>
        private static void ResultOfMethod(MyQueue queue, string headLine)
        {
            Console.WriteLine(headLine);
            foreach (var e in queue)
                Console.WriteLine(e);
            Console.WriteLine();
        }

        /// <summary>
        /// Cоздаёт список слов по содержимому словаря, заданного текстовым файлом
        /// </summary>
        /// <param name="path">Расположение файла</param>
        public static MyQueue Coding(string path)
        {
            var queue = new MyQueue();
            using (var sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var pair = line.Split('-');
                    queue.Add(new Pair(pair[0], pair[1]));
                }
            }

            return queue;
        }

        /// <summary>
        /// Восстанавливает множество слов словаря и выводит результат в текстовый файл
        /// </summary>
        /// <param name="queue">Список пар слов, предназначенный для декодирования</param>
        /// <param name="path">Расположение файла</param>
        public static void Decoding(MyQueue queue, string path)
        {
            using (var sr = new StreamWriter(path, false))
            {
                Pair pair;
                while (true)
                {
                    try
                    {
                        pair = queue.Dequeue();
                    }
                    catch (InvalidOperationException)
                    {
                        return;
                    }
                    sr.WriteLine(pair);
                }
            }
        }

        /// <summary>
        /// Переводит текст из текстового файла и выводит его на консоль
        /// </summary>
        /// <param name="path">Расположение файла</param>
        /// <param name="dictionary">Список слов MyQueue</param>
        /// <param name="write">Функция вывода на консоль</param>
        public static void Translate(string path, MyQueue dictionary, Action<string> write)
        {
            using (var sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine().Split(' ');
                    foreach (var e in line)
                        write(dictionary.FindTranslate(e) + ' ');
                    write("\r\n");
                }
            }
        }
    }
}
