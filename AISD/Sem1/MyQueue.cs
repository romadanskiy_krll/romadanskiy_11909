﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Sem1
{
    /// <summary>
    /// Односвязный список слов Pair, реализованный по принципу очереди
    /// </summary>
    public class MyQueue : IEnumerable<Pair>
    {
        private MyQueueItem head;
        private MyQueueItem tail;
        private int count1;

        /// <summary>
        /// Число элементов списка, которые при переводе изменяют длину слова не более чем на единицу
        /// </summary>
        public int Count1 { get { return count1; } }

        /// <summary>
        /// Проверяет, изменятеся ли длина слова при переводе не более чем на единицу
        /// </summary>
        /// <returns>
        /// Возвращает true, если длина слова при переводе изменяется не более чем на единицу, и false в противном случае
        /// </returns>
        private bool DiffenceInLengthsIsNotMoreThan1(string word, string translate)
        {
            return Math.Abs(word.Length - translate.Length) <= 1;
        }

        /// <summary>
        /// Вставляет элемент в конец списка
        /// </summary>
        public void Add(Pair pair)
        {
            if (head == null)
                tail = head = new MyQueueItem { Value = pair, Next = null };
            else
            {
                var item = new MyQueueItem { Value = pair, Next = null };
                tail.Next = item;
                tail = item;
            }
            if (DiffenceInLengthsIsNotMoreThan1(pair.Word, pair.Translate))
                count1++;
        }

        // Метод изпользуется для декодирования
        /// <summary>
        /// Удаляет объект из начала очереди и возвращает его
        /// </summary>
        public Pair Dequeue()
        {
            if (head == null) throw new InvalidOperationException();
            var result = head.Value;
            head = head.Next;
            if (head == null) tail = null;
            if (DiffenceInLengthsIsNotMoreThan1(result.Word, result.Translate))
                count1--;

            return result;
        }

        /// <summary>
        /// Удаляет эдемент из списка
        /// </summary>
        /// <param name="word">Cлово на исходном языке</param>
        public void Remove(string word)
        {
            if (head == null) return;
            if (head.Value.Word == word)
            {
                if (DiffenceInLengthsIsNotMoreThan1(head.Value.Word, head.Value.Translate))
                    count1--;
                head = head.Next;
                return;
            }
            var prev = head;
            var current = head.Next;
            while(current != null)
            {
                if (current.Value.Word == word)
                {
                    if (DiffenceInLengthsIsNotMoreThan1(current.Value.Word, current.Value.Translate))
                        count1--;
                    prev.Next = current.Next;
                    return;
                }
                prev = current;
                current = current.Next;
            }
        }

        /// <summary>
        /// Формирует новый список MyQueue пар слов, которые имеют уникальный перевод
        /// </summary>
        public MyQueue CreateUniqueQueue()
        {
            var uniqueQueue = new MyQueue();
            var dict = new Dictionary<string, Pair>();
            foreach (var pair in this)
            {
                if (!dict.ContainsKey(pair.Translate))
                {
                    dict.Add(pair.Translate, pair);
                    uniqueQueue.Add(pair);
                }
                else
                    uniqueQueue.Remove(dict[pair.Translate].Word);
            }

            return uniqueQueue;
        }

        /// <summary>
        /// Находит слово в списке и возвращает его перевод
        /// </summary>
        /// <param name="word">Слово на исходном языке</param>
        /// <returns>
        /// Возвращает перевод слова, если оно найдено, иначе возвращает исходное слово
        /// </returns>
        public string FindTranslate(string word)
        {
            foreach (var pair in this)
            {
                if (pair.Word == word) return pair.Translate;
            }

            return word;
        }

        public IEnumerator<Pair> GetEnumerator()
        {
            var current = head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
