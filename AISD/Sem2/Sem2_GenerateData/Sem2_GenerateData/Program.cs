﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Sem2_GenerateData
{
    class Program
    {
        public static void Main()
        {
            var verticesNum = 15;
            for (var i = 0; i < 50; i++)
            {
                GenerateFullGraph(verticesNum, string.Format("Data_{0}.txt", i));
                verticesNum += 2;
            }
        }

        /// <summary>
        /// Сгенерировать полный связный граф и выпести его в текстовый файл
        /// </summary>
        public static void GenerateFullGraph(int verticesNum, string path)
        {
            using var sr = new StreamWriter(path, false);
            sr.WriteLine(verticesNum);
            var edgesNum = verticesNum * (verticesNum - 1) / 2;
            sr.WriteLine(edgesNum);
            var hash = new HashSet<Tuple<int, int>>();
            for (var i = 0; i < verticesNum; i++)
                for (var j = 0; j < verticesNum; j++)
                {
                    var rnd = new Random();
                    if (j != i && !hash.Contains(Tuple.Create(i, j)))
                    {
                        sr.WriteLine(new Edge(i, j, rnd.Next(1, 100)));
                        hash.Add(Tuple.Create(i, j));
                        hash.Add(Tuple.Create(j, i));
                    }
                }
            sr.Close();
        }
    }
}
