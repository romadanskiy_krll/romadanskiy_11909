﻿using System;

namespace Sem2
{
    public static class ArrayExtension
    {
        private static Edge[] temporaryArray;

        /// <summary>
        /// Сортировать массив слиянием
        /// </summary>
        public static void MergeSort(this Edge[] array, ref int iterationsCounter) // O(N* log(N))
        {
            temporaryArray = new Edge[array.Length];
            MergeSort(array, 0, array.Length - 1, ref iterationsCounter);
        }

        private static void MergeSort(Edge[] array, int start, int end, ref int iterationsCounter)
        {
            if (start == end)
                return;
            var middle = (start + end) / 2;
            MergeSort(array, start, middle, ref iterationsCounter);
            MergeSort(array, middle + 1, end, ref iterationsCounter);
            Merge(array, start, middle, end, ref iterationsCounter);
        }

        private static void Merge(Edge[] array, int start, int middle, int end, ref int iterationsCounter)
        {
            var leftPtr = start;
            var rightPtr = middle + 1;
            var length = end - start + 1;
            for (int i = 0; i < length; i++)
            {
                if (rightPtr > end || (leftPtr <= middle && array[leftPtr].Weight < array[rightPtr].Weight))
                {
                    temporaryArray[i] = array[leftPtr];
                    leftPtr++;
                }
                else
                {
                    temporaryArray[i] = array[rightPtr];
                    rightPtr++;
                }
                iterationsCounter++;
            }
            for (int i = 0; i < length; i++)
                array[i + start] = temporaryArray[i];
        }
    }
}
