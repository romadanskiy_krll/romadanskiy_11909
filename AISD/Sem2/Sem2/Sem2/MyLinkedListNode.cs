﻿using System;

namespace Sem2
{
    /// <summary>
    /// Узел односвязного списка MyLinkedList
    /// </summary>
    public class MyLinkedListNode
    {
        public Edge Value { get; set; }
        public MyLinkedListNode Next { get; set; }
    }
}
