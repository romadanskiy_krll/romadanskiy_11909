﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;

namespace Sem2
{
    class Program
    {
        public static void Main()
        {
            for (var i = 0; i < 50; i++)
            {
                var data = GetData(string.Format("Data_{0}.txt", i));
                var verticesNum = data.Item1;
                var edgesNum = data.Item2;

                var arrayGraph = data.Item3.ToArray();
                var myStopwatch1 = new Stopwatch();
                var iterationsCounter1 = 0;
                myStopwatch1.Start();
                Kruskal(arrayGraph, verticesNum, ref iterationsCounter1);
                myStopwatch1.Stop();
                var time1 = myStopwatch1.Elapsed;

                var linkedListGraph = data.Item3.ToMyLinkedList();
                var myStopwatch2 = new Stopwatch();
                var iterationsCounter2 = 0;
                myStopwatch2.Start();
                Kruskal(linkedListGraph, verticesNum, ref iterationsCounter2);
                myStopwatch2.Stop();
                var time2 = myStopwatch2.Elapsed;

                Decoding(Tuple.Create(edgesNum, time1.TotalMilliseconds), "ArrayTime.txt");
                Decoding(Tuple.Create(edgesNum, time2.TotalMilliseconds), "LinkedListTime.txt");
                // Измерения времени и итераций производились отдельно
                Decoding(Tuple.Create(edgesNum, (double)iterationsCounter1), "ArrayIterations.txt");
                Decoding(Tuple.Create(edgesNum, (double)iterationsCounter2), "LinkedListIterations.txt");
            }
        }

        /// <summary>
        /// Алгоритм Краскала для входных данных типа Array
        /// </summary>
        public static List<Edge> Kruskal(Edge[] edges, int verticesNum, ref int iterationsCounter)
        {
            var tree = new List<Edge>();
            edges.MergeSort(ref iterationsCounter);

            var dsu = new Dsu(verticesNum);

            for (var i = 0; i < verticesNum; i++)
            {
                dsu.MakeSet(i);
                iterationsCounter++;
            }

            foreach (var edge in edges)
            {
                if (dsu.Find(edge.From) != dsu.Find(edge.To))
                {
                    tree.Add(edge);
                    dsu.Union(edge.From, edge.To);
                }

                iterationsCounter++;

                if (tree.Count == verticesNum - 1)
                    break;
            }

            return tree;
        }

        /// <summary>
        /// Алгоритм Краскала для входных данных типа MyLinkedList
        /// </summary>
        public static List<Edge> Kruskal(MyLinkedList edges, int verticesNum, ref int iterationsCounter)
        {
            var tree = new List<Edge>();
            edges.MergeSort(ref iterationsCounter);

            var dsu = new Dsu(verticesNum);

            for (var i = 0; i < verticesNum; i++)
            {
                dsu.MakeSet(i);
                iterationsCounter++;
            }

            foreach (var edge in edges)
            {
                if (dsu.Find(edge.From) != dsu.Find(edge.To))
                {
                    tree.Add(edge);
                    dsu.Union(edge.From, edge.To);
                }

                iterationsCounter++;

                if (tree.Count == verticesNum - 1)
                    break;
            }

            return tree;
        }

        /// <summary>
        /// Получить информацию о графе из текстового файла
        /// </summary>
        public static Tuple<int, int, List<Edge>> GetData(string path)
        {
            var graph = new List<Edge>();
            var verticesNum = 0;
            var edgesNum = 0;
            using (var sr = new StreamReader(path))
            {
                verticesNum = int.Parse(sr.ReadLine());
                edgesNum = int.Parse(sr.ReadLine());
                while (!sr.EndOfStream)
                {
                    var edge = sr.ReadLine().Split(' ').Select(i => int.Parse(i)).ToArray();
                    graph.Add(new Edge(edge[0], edge[1], edge[2]));
                }
            }

            return Tuple.Create(verticesNum, edgesNum, graph);
        }

        /// <summary>
        /// Вывести результат тестирования в текстовый файл
        /// </summary>
        public static void Decoding(Tuple<int, double> data, string path)
        {
            using var sr = new StreamWriter(path, true);
            sr.WriteLine(string.Format("{0} {1}", data.Item1, data.Item2));
        }
    }
}
