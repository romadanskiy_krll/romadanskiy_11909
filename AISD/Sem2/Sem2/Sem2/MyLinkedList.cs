﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Sem2
{
    /// <summary>
    /// Односвязный список
    /// </summary>
    public class MyLinkedList : IEnumerable<Edge>
    {
        private MyLinkedListNode head;
        private MyLinkedListNode tail;

        /// <summary>
        /// Вставить элемент в конец списка
        /// </summary>
        public void Add(Edge pair)
        {
            if (head == null)
                tail = head = new MyLinkedListNode { Value = pair, Next = null };
            else
            {
                var item = new MyLinkedListNode { Value = pair, Next = null };
                tail.Next = item;
                tail = item;
            }
        }

        public IEnumerator<Edge> GetEnumerator()
        {
            var current = head;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Сортировать связный список слиянием
        /// </summary>
        public void MergeSort(ref int iterationsCounter) // O(N*log(N))
        {
            this.head = MergeSort(this.head, ref iterationsCounter);
        }

        private MyLinkedListNode MergeSort(MyLinkedListNode curHead, ref int iterationsCounter)
        {
            if (curHead == null || curHead.Next == null)
                return curHead;

            // Находим середину списка
            var middle = GetMiddle(curHead, ref iterationsCounter);
            var nextofmiddle = middle.Next;

            // Устанавливаем для серединего узла указатель поля next на null
            // Тем самым получаем левую половину списка
            middle.Next = null;

            // Применяем сортировку слиянием для левой половины списка
            var left = MergeSort(curHead, ref iterationsCounter);
            // Применяем сортировку слиянием для правой половины списка
            var right = MergeSort(nextofmiddle, ref iterationsCounter);

            // Объединяем левый и правый списки
            var sortedlist = Merge(left, right, ref iterationsCounter);
            return sortedlist;
        }

        private MyLinkedListNode Merge(MyLinkedListNode a, MyLinkedListNode b, ref int iterationsCounter)
        {
            var result = new MyLinkedListNode();

            if (a == null)
                return b;
            if (b == null)
                return a;

            if (a.Value.Weight <= b.Value.Weight)
            {
                result = a;
                iterationsCounter++;
                result.Next = Merge(a.Next, b, ref iterationsCounter);
            }
            else
            {
                result = b;
                iterationsCounter++;
                result.Next = Merge(a, b.Next, ref iterationsCounter);
            }

            return result;
        }
       
        /// <summary>
        /// Получить средний узел связного списка
        /// </summary>
        private MyLinkedListNode GetMiddle(MyLinkedListNode curHead, ref int iterationsCounter)
        { 
            if (curHead == null)
                return curHead;

            var fastptr = curHead.Next;
            var slowptr = curHead;

            // Перемещаем fastptr на два, а slowptr на один.
            // В итоге slowptr будет указывать на средний узел
            while (fastptr != null)
            {
                fastptr = fastptr.Next;
                if (fastptr != null)
                {
                    slowptr = slowptr.Next;
                    fastptr = fastptr.Next;
                }
                iterationsCounter++;
            }

            return slowptr;
        }
    }


    /// <summary>
    /// Создать связный список рёбер MyLinkedList из IEnumerable<Edge>
    /// </summary>
    public static class IEnumerableExtension
    {
        public static MyLinkedList ToMyLinkedList(this IEnumerable<Edge> edges)
        {
            var list = new MyLinkedList();
            foreach (var edge in edges)
                list.Add(edge);

            return list;
        }
    }
}
