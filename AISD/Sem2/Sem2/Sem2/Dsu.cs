﻿using System;

namespace Sem2
{
    /// <summary>
    /// Cистема непересекающихся множеств
    /// </summary>
    public class Dsu
    {
        private readonly int[] parent;
        private readonly int[] rank;

        public Dsu (int verticesNum)
        {
            parent = new int[verticesNum];
            rank = new int[verticesNum];
        }

        public void MakeSet(int x) // O(1)
        {
            parent[x] = x;
            rank[x] = 0;
        }

        public int Find(int x) // O(1)
        {
            if (parent[x] == x)
                return x;
            return parent[x] = Find(parent[x]);
        }

        public void Union(int x, int y) // O(1)
        {
            x = Find(x);
            y = Find(y);
            if (rank[x] < rank[y])
                parent[x] = y;
            else
            {
                parent[y] = x;
                if (rank[x] == rank[y])
                    ++rank[x];
            }
        }
    }
}
