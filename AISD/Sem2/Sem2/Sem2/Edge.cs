﻿using System;

namespace Sem2
{
    /// <summary>
    /// Ребро графа
    /// </summary>
    public class Edge
    {
        public int From;
        public int To;
        public int Weight;

        public Edge(int from, int to, int weight)
        {
            From = from;
            To = to;
            Weight = weight;
        }

        public override string ToString()
        {
            return string.Format("{0} - {1} : {2}", From, To, Weight);
        }
    }
}
