﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;
using NUnit.Framework;

namespace FirstGame
{
    public partial class Form1 : Form
    {
        int playerSpeed;
        WindowsMediaPlayer gameSong;
        int cloudSpeed;
        PictureBox[] clouds;
        int enemiesSpeed;
        PictureBox[] enemies;
        int points;
        Label pointsLabel;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var rnd = new Random();

            playerSpeed = 5;

            points = 0;
            pointsLabel = new Label
            {
                Location = new Point(10, 10),
                Text = "Счёт: " + points
            };
            Controls.Add(pointsLabel);

            cloudSpeed = 3;
            clouds = new PictureBox[20];
            MakeClouds(rnd);

            enemiesSpeed = 3;
            enemies = new PictureBox[9];
            for (var i = 0; i < enemies.Length; i++)
            {
                enemies[i] = MakeNewEnemy(i, rnd);
                Controls.Add(enemies[i]);
            }

            gameSong = new WindowsMediaPlayer { URL = "media\\gameSong.mp3" };
            gameSong.settings.setMode("loop", true);
            gameSong.settings.volume = 15;
            gameSong.controls.play();
        }

        private void MakeClouds(Random rnd)
        {
            for (int i = 0; i < clouds.Length; i++)
            {
                clouds[i] = new PictureBox
                {
                    BorderStyle = BorderStyle.None,
                    Location = new Point(rnd.Next(-100, 1280), rnd.Next(50, 320))
                };
                if (i % 2 == 1)
                {
                    clouds[i].Size = new Size(rnd.Next(100, 225), rnd.Next(30, 70));
                    clouds[i].BackColor = Color.FromArgb(rnd.Next(50, 125), 255, 250, 240);
                }
                else
                {
                    clouds[i].Size = new Size(rnd.Next(130, 160), rnd.Next(20, 30));
                    clouds[i].BackColor = Color.FromArgb(rnd.Next(100, 200), 255, 255, 255);
                }
                Controls.Add(clouds[i]);
            }
        }

        private void MoveEnemies()
        {
            var rnd = new Random();
            for (var i = 0; i < enemies.Length; i++)
            {
                enemies[i].Left -= enemiesSpeed + (int)(Math.Sin(enemies[i].Left * Math.PI / 180) + Math.Cos(enemies[i].Left * Math.PI / 180));
                Intersect();
                if (enemies[i].Right < 0)
                {
                    GetPoint();
                    var newEnemy = MakeNewEnemy(i, rnd);
                    enemies[i].Size = newEnemy.Size;
                    enemies[i].Location = newEnemy.Location;
                    enemies[i].Image = newEnemy.Image;
                }
            }
        }

        private PictureBox MakeNewEnemy(int index, Random rnd)
        {
            var enemy = new PictureBox();
            var size = rnd.Next(30, 60);
            enemy.Size = new Size(size, size);
            enemy.SizeMode = PictureBoxSizeMode.Zoom;
            enemy.Image = Image.FromFile(string.Format("pictures\\enemy{0}.png", rnd.Next() % 2));
            enemy.Location = new Point((index + 3) * rnd.Next(190, 240) + 650, rnd.Next(438, 658));
            return enemy;
        }

        private void Timer1_Tick_1(object sender, EventArgs e)
        {
            for (var i = 0; i < clouds.Length; i++)
            {
                clouds[i].Left += cloudSpeed;
                if (clouds[i].Left >= 1280)
                    clouds[i].Left = -200;
            }
        }

        private void MoveLeftTimer_Tick(object sender, EventArgs e)
        {
            if (player.Left > 10)
                player.Left -= playerSpeed;
        }

        private void MoveRightTimer_Tick(object sender, EventArgs e)
        {
            if (player.Left < 1150)
                player.Left += playerSpeed;
        }

        private void MoveUpTimer_Tick(object sender, EventArgs e)
        {
            if (player.Top > 400)
                player.Top -= playerSpeed;
        }

        private void MoveDownTimer_Tick(object sender, EventArgs e)
        {
            if (player.Top < 600)
                player.Top += playerSpeed;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    MoveLeftTimer.Start();
                    break;
                case Keys.Right:
                    MoveRightTimer.Start();
                    break;
                case Keys.Up:
                    MoveUpTimer.Start();
                    break;
                case Keys.Down:
                    MoveDownTimer.Start();
                    break;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    MoveLeftTimer.Stop();
                    break;
                case Keys.Right:
                    MoveRightTimer.Stop();
                    break;
                case Keys.Up:
                    MoveUpTimer.Stop();
                    break;
                case Keys.Down:
                    MoveDownTimer.Stop();
                    break;
            }
        }

        private void EnemiesTimer_Tick(object sender, EventArgs e)
        {
            MoveEnemies();
        }

        private void GetPoint()
        {
            points++;
            pointsLabel.Text = string.Format("Счёт: {0}", points);
        }

        private void Intersect()
        {
            for (var i = 0; i < enemies.Length; i++)
            {
                if (player.Bounds.IntersectsWith(enemies[i].Bounds))
                {
                    StopAllTimers();
                    var res = MessageBox.Show("Кажется, Вам задали слишком много дз ((( \r\nВаш счёт: " + points, "Начать заново?", MessageBoxButtons.RetryCancel);

                    switch (res)
                    {
                        case DialogResult.Retry:
                            Application.Restart();
                            Environment.Exit(0);
                            break;
                        default:
                            Environment.Exit(0);
                            break;
                    }
                }
            }
        }

        private void StopAllTimers()
        {
            Timer1.Stop();
            MoveLeftTimer.Stop();
            MoveRightTimer.Stop();
            MoveUpTimer.Stop();
            MoveDownTimer.Stop();
            EnemiesTimer.Stop();
            gameSong.controls.stop();
        }
    }
}
