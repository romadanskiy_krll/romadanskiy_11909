﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FirstGame
{
    // Всё-таки для начала воспользовался конструктором, так как не доконца разобрался, что и как здесь устроено.

    // Протестировать свои методы у меня не получилось, а точнее, не понял, как именно их тестировать. Да и времени вообще на это не было.

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
