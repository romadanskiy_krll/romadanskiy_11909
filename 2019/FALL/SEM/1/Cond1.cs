﻿using System;

namespace Cond1
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("Введите начальную клетку (пример: a1)");
            string from = Console.ReadLine();
            Console.WriteLine("Введите конечную клетку (пример: d4)");
            string to = Console.ReadLine();
            Console.WriteLine();
            TestMove(from, to);
        }

        public static void TestMove(string from, string to)
        {
            var dx = Math.Abs(to[0] - from[0]); //смещение фигуры по горизонтали
            var dy = Math.Abs(to[1] - from[1]); //смещение фигуры по вертикали
            Console.WriteLine("From {0} to {1}:", from, to);
            Console.WriteLine("For Bishop (слон): {0}", IsCorrectMoveForBishop(dx, dy));
            Console.WriteLine("For Knight (конь): {0}", IsCorrectMoveForKnight(dx, dy));
            Console.WriteLine("For Rook (ладья): {0}", IsCorrectMoveForRook(dx, dy));
            Console.WriteLine("For Queen (ферзь): {0}", IsCorrectMoveForQueen(dx, dy));
            Console.WriteLine("For King (король): {0}", IsCorrectMoveForKing(dx, dy));
        }

        public static bool IsCorrectMoveForBishop(int dx, int dy) // Слон 
        {
            return (dx == dy && dx != 0 && dy != 0);
        }

        public static bool IsCorrectMoveForKnight(int dx, int dy) // Конь 
        {
            return (dx == 1 && dy == 2) || (dx == 2 && dy ==1);
        }

        public static bool IsCorrectMoveForRook(int dx, int dy) // Ладья 
        {
            return (dx == 0 && dy != 0) || (dx != 0 && dy == 0);
        }

        public static bool IsCorrectMoveForQueen(int dx, int dy) // Ферзь
        {
            return (dx == dy && dx != 0 && dy != 0) || (dx == 0 && dy != 0) || (dy == 0 && dx != 0);
        }

        public static bool IsCorrectMoveForKing(int dx, int dy) // Король
        {
            return (dx == dy && dx == 1 && dy == 1) || (dx == 1 && dy == 0) || (dx == 0 && dy == 1);
        }
    }
}

