﻿using System;
using System.Collections.Generic;

namespace Game21
{
    public enum Suit { Hearts = 0, Diamonds = 1, Clubs = 2, Spades = 3 }

    public class Card
    {
        public Suit Suit;
        public int Value;
    }

    public class Deck
    {
        public static List<Card> deck; 

        public void CreateDeck() // Создаём колоду
        {
            for (int i=0; i<4; i++)
                for (int j=2; j<=14; j++)
                {
                    var value = j;
                    if (j == 11 || j == 12 || j == 13) value = 10;
                    if (j == 14) 
                    {
                        var rnd = new Random();
                        var n = rnd.Next(0, 1);
                        if (n == 0) value = 1;
                        if (n == 1) value = 11;
                    }
                    deck.Add(new Card() { Suit = (Suit)i, Value = value });
                }
        }

        public void MakeShuffle() // Перемешиваем колоду
        {
            var rnd = new Random();
            for (int i = deck.Count - 1; i >= 1; i--)
            {
                int j = rnd.Next(i + 1);
                var temp = deck[j];
                deck[j] = deck[i];
                deck[i] = temp;
            }
        }

        public Card GetCard() // Берём верхнюю карту из колоды
        {
            var card = deck[deck.Count-1];
            deck.RemoveAt(deck.Count - 1);
            return card;
        }
    }



    class Program
    {
        public static int sum1;
        public static int sum2;
        static void Main()
        {
            var deck = new Deck();
            deck.CreateDeck();
            deck.MakeShuffle();
            for(int i=0; i<52; i++)
            {
                if (i % 2 == 0)
                {
                    var card = deck.GetCard();
                    sum1 += card.Value;
                }
                else 
                {
                    var card = deck.GetCard();
                    sum1 += card.Value;
                }

                if (sum1 >= 21) { Console.WriteLine("1 is winner"); break; }
                if (sum2 >= 21) { Console.WriteLine("2 is winner"); break; }
            }
        }
    }
}
