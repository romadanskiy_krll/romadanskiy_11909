﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("Введите текст:");
            var text = Console.ReadLine();
            //var text = "5685-00 (5000 подсобники); 35750-00 (БОЯРИН);";
            //var text = "5(1(2)3)6";

            var textBuilder = GetTextWithoutBkt(text);

            var extraSymbols = new List<char>();
            FindExtraSymbols(textBuilder, extraSymbols);

            Console.WriteLine("\nНайденные числа:");
            var numbersArray = textBuilder.ToString().Split(extraSymbols.ToArray(), StringSplitOptions.RemoveEmptyEntries);
            var sum = 0;
            foreach (var pair in numbersArray)
            {
                var number = pair.Split('-')[0];
                Console.Write("{0} \t", number);
                sum += int.Parse(number);
            }
            Console.WriteLine("\n\nРезультат:");
            Console.WriteLine(sum);

            Console.ReadKey();
        }

        private static void FindExtraSymbols(StringBuilder textBuilder, List<char> extraSymbols)
        {
            for (int i = 0; i < textBuilder.Length; i++)
            {
                var symbol = textBuilder[i];
                if (!extraSymbols.Contains(symbol))
                {
                    if (!char.IsNumber(symbol) && symbol != '-')
                        extraSymbols.Add(symbol);
                }
            }
        }

        private static StringBuilder GetTextWithoutBkt(string text)
        {
            var textBuilder = new StringBuilder();
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '(')
                {
                    i = GetIndex(i, text);
                    if (i < text.Length)
                        textBuilder.Append(" " + text[i]);
                }
                else
                    textBuilder.Append(text[i]);
            }

            return textBuilder;
        }

        public static int GetIndex(int i, string text)
        {
            while (text[i] != ')')
            {
                ++i;
                if (text[i] == '(')
                    i = GetIndex(i, text);
            }
            ++i;

            return i;
        }
    }
}
